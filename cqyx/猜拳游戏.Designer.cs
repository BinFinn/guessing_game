﻿namespace cqyx
{
    partial class 猜拳游戏
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnST = new System.Windows.Forms.Button();
            this.btnJD = new System.Windows.Forms.Button();
            this.btnB = new System.Windows.Forms.Button();
            this.btnWJ = new System.Windows.Forms.Button();
            this.btnDN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblDNS = new System.Windows.Forms.Label();
            this.lblWJS = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnST
            // 
            this.btnST.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnST.BackColor = System.Drawing.Color.Transparent;
            this.btnST.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnST.FlatAppearance.BorderSize = 0;
            this.btnST.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnST.Image = global::cqyx.Properties.Resources.石头;
            this.btnST.Location = new System.Drawing.Point(91, 298);
            this.btnST.Margin = new System.Windows.Forms.Padding(0);
            this.btnST.MaximumSize = new System.Drawing.Size(115, 115);
            this.btnST.MinimumSize = new System.Drawing.Size(115, 115);
            this.btnST.Name = "btnST";
            this.btnST.Size = new System.Drawing.Size(115, 115);
            this.btnST.TabIndex = 0;
            this.btnST.Tag = "1";
            this.btnST.UseVisualStyleBackColor = false;
            this.btnST.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnJD
            // 
            this.btnJD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJD.BackColor = System.Drawing.Color.Transparent;
            this.btnJD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnJD.FlatAppearance.BorderSize = 0;
            this.btnJD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJD.Image = global::cqyx.Properties.Resources.剪刀;
            this.btnJD.Location = new System.Drawing.Point(285, 298);
            this.btnJD.Margin = new System.Windows.Forms.Padding(0);
            this.btnJD.MaximumSize = new System.Drawing.Size(115, 115);
            this.btnJD.MinimumSize = new System.Drawing.Size(115, 115);
            this.btnJD.Name = "btnJD";
            this.btnJD.Size = new System.Drawing.Size(115, 115);
            this.btnJD.TabIndex = 1;
            this.btnJD.Tag = "2";
            this.btnJD.UseVisualStyleBackColor = false;
            this.btnJD.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnB
            // 
            this.btnB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnB.BackColor = System.Drawing.Color.Transparent;
            this.btnB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnB.FlatAppearance.BorderSize = 0;
            this.btnB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnB.Image = global::cqyx.Properties.Resources.布;
            this.btnB.Location = new System.Drawing.Point(479, 298);
            this.btnB.Margin = new System.Windows.Forms.Padding(0);
            this.btnB.MaximumSize = new System.Drawing.Size(115, 115);
            this.btnB.MinimumSize = new System.Drawing.Size(115, 115);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(115, 115);
            this.btnB.TabIndex = 2;
            this.btnB.Tag = "3";
            this.btnB.UseVisualStyleBackColor = false;
            this.btnB.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnWJ
            // 
            this.btnWJ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWJ.BackColor = System.Drawing.Color.Transparent;
            this.btnWJ.FlatAppearance.BorderSize = 0;
            this.btnWJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWJ.Location = new System.Drawing.Point(200, 61);
            this.btnWJ.Margin = new System.Windows.Forms.Padding(0);
            this.btnWJ.MaximumSize = new System.Drawing.Size(115, 115);
            this.btnWJ.MinimumSize = new System.Drawing.Size(115, 115);
            this.btnWJ.Name = "btnWJ";
            this.btnWJ.Size = new System.Drawing.Size(115, 115);
            this.btnWJ.TabIndex = 3;
            this.btnWJ.UseVisualStyleBackColor = false;
            // 
            // btnDN
            // 
            this.btnDN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDN.BackColor = System.Drawing.Color.Transparent;
            this.btnDN.FlatAppearance.BorderSize = 0;
            this.btnDN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDN.Location = new System.Drawing.Point(370, 61);
            this.btnDN.Margin = new System.Windows.Forms.Padding(0);
            this.btnDN.MaximumSize = new System.Drawing.Size(115, 115);
            this.btnDN.MinimumSize = new System.Drawing.Size(115, 115);
            this.btnDN.Name = "btnDN";
            this.btnDN.Size = new System.Drawing.Size(115, 115);
            this.btnDN.TabIndex = 4;
            this.btnDN.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(236, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 22);
            this.label1.TabIndex = 5;
            this.label1.Text = "玩家";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(406, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 22);
            this.label2.TabIndex = 6;
            this.label2.Text = "电脑";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.BackColor = System.Drawing.Color.Transparent;
            this.lblResult.Font = new System.Drawing.Font("微软雅黑", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblResult.Location = new System.Drawing.Point(280, 204);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(125, 46);
            this.lblResult.TabIndex = 7;
            this.lblResult.Text = "请出拳";
            // 
            // lblDNS
            // 
            this.lblDNS.AutoSize = true;
            this.lblDNS.BackColor = System.Drawing.Color.Transparent;
            this.lblDNS.Font = new System.Drawing.Font("微软雅黑", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblDNS.Location = new System.Drawing.Point(552, 102);
            this.lblDNS.Name = "lblDNS";
            this.lblDNS.Size = new System.Drawing.Size(42, 22);
            this.lblDNS.TabIndex = 9;
            this.lblDNS.Text = "电脑";
            // 
            // lblWJS
            // 
            this.lblWJS.AutoSize = true;
            this.lblWJS.BackColor = System.Drawing.Color.Transparent;
            this.lblWJS.Font = new System.Drawing.Font("微软雅黑", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblWJS.Location = new System.Drawing.Point(99, 102);
            this.lblWJS.Name = "lblWJS";
            this.lblWJS.Size = new System.Drawing.Size(42, 22);
            this.lblWJS.TabIndex = 10;
            this.lblWJS.Text = "玩家";
            // 
            // 猜拳游戏
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::cqyx.Properties.Resources.bj;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(684, 431);
            this.Controls.Add(this.lblWJS);
            this.Controls.Add(this.lblDNS);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDN);
            this.Controls.Add(this.btnWJ);
            this.Controls.Add(this.btnB);
            this.Controls.Add(this.btnJD);
            this.Controls.Add(this.btnST);
            this.Name = "猜拳游戏";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "猜拳游戏";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnST;
        private System.Windows.Forms.Button btnJD;
        private System.Windows.Forms.Button btnB;
        private System.Windows.Forms.Button btnWJ;
        private System.Windows.Forms.Button btnDN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblDNS;
        private System.Windows.Forms.Label lblWJS;
    }
}

