﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cqyx
{
    public partial class 猜拳游戏 : Form
    {
        public 猜拳游戏()
        {
            InitializeComponent();
        }

        //玩家出拳
        int wj = 0;
        private void btnAll_Click(object sender, EventArgs e)
        {
            Button btn = (Button)(sender);
            if (Convert.ToString(btn.Tag) == "1")
            {
                btnWJ.Image = btnST.Image;
            }
            else if (Convert.ToString(btn.Tag) == "2")
            {
                btnWJ.Image = btnJD.Image;
            }
            else
            {
                btnWJ.Image = btnB.Image;
            }
            wj = Convert.ToInt32(btn.Tag);
            sys_Give();
        }

        //电脑出拳
        int sys = 0;
        public void sys_Give()
        {
            Random rdm = new Random();
            sys = rdm.Next(1, 4);
            if (sys == 1)
            {
                btnDN.Image = btnST.Image;
            }
            else if (sys == 2)
            {
                btnDN.Image = btnJD.Image;
            }
            else
            {
                btnDN.Image = btnB.Image;
            }
            compare_Result();
        }

        //结果比较
        int wjs = 0;
        int dns = 0;
        public void compare_Result()
        {
            if (sys == wj)
            {
                MessageBox.Show("平局");
            }
            else if (sys > wj && sys - wj == 1)
            {
                MessageBox.Show("你赢了！");
                wjs++;
            }
            else if (sys < wj && wj - sys == 2)
            {
                MessageBox.Show("你赢了！");
                wjs++;
            }
            else
            {
                MessageBox.Show("你输了！");
                dns++;
            }
            lblWJS.Text = wjs.ToString();
            lblDNS.Text = dns.ToString();
        }
    }
}
